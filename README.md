xdo module for Python
=====================

python-xdo is a module for accessing libxdo from python.

It is based on version 3 of the xdo API, which is a nicely-organized
API.  It won't work against earlier SONAME versions of libxdo.

python-xdo was created as a minimal binding use in a couple dedicated
python applications -- as such, it does currently attempt to cover the
full libxdo api.  It's very minimal (just enough for the job at hand
currently), but it doesn't need to remain that way.

Patches to improve library coverage are welcome!

Bug Reports, Patches, and Questions
-----------------------------------

Discussions about libxdo and python-xdo are welcome at
xdotool-users@googlegroups.com (you may need to subscribe to post).

The source code for python-xdo is published on 
[gitlab](https://gitlab.com/dkg/python-xdo)

And the xdo bindings can also be found at [PyPI](https://pypi.org/project/xdo/)

Credits
-------

libxdo itself comes from Jordan Sissel:

 - https://www.semicomplete.com/projects/xdotool/
 - https://github.com/jordansissel/xdotool

The python-xdo binding was initially written by
Daniel Kahn Gillmor <dkg@fifthhorseman.net>

The overhaul to ctypes was inspired by discussion with (and large
parts of the ctypes code originated from) Samuele Santi
<redshadow@hackzine.org>, and additional implementation work was done
by Cyril Brulebois <cyril@debamax.com>.
